CREATE TABLE IF NOT EXISTS clients (
    id SERIAL PRIMARY KEY,
    name VARCHAR(50),
    surname VARCHAR(50),
    country VARCHAR(50)
);

INSERT INTO clients (name, surname, country) VALUES ('Jorge','Sola','Spain');
INSERT INTO clients (name, surname, country) VALUES ('Pablo','Viñuales','Spain');
INSERT INTO clients (name, surname, country) VALUES ('Tiago','Ferreira','Portugal');
INSERT INTO clients (name, surname, country) VALUES ('Manuel','Sola','Spain');