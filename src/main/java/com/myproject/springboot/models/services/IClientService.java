package com.myproject.springboot.models.services;

import com.myproject.springboot.models.dao.IClientDao;
import com.myproject.springboot.models.entity.Client;

import java.util.List;

public interface IClientService {

    public List<Client> findAll();
    public  Client findOne(Long id);
    public void save(Client client);
    public void delete(Long id);
}
