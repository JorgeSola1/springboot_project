package com.myproject.springboot.controllers;

import com.myproject.springboot.models.dao.ClientDaoImpl;
import com.myproject.springboot.models.dao.IClientDao;
import com.myproject.springboot.models.entity.Client;
import com.myproject.springboot.models.services.ClientServiceImpl;
import com.myproject.springboot.models.services.IClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;

import javax.validation.Valid;
import java.util.List;

@Controller
@SessionAttributes("client")
public class ClientControllers {

    @Autowired
    IClientService clientService;

    @GetMapping(value="/home")
    public String homePage(){
        return "homePage";
    }

    @GetMapping(value="/listClients")
    public String getClients(Model model){
        List<Client> clients = clientService.findAll();
        model.addAttribute("title","Clients");
        model.addAttribute("clients", clients);
        return "list";
    }

    @GetMapping(value = "/form")
    public String createClient(Model model){

        Client client = new Client();
        model.addAttribute("client",client);
        model.addAttribute("title","Client form");
        return "form";
    }

    @PostMapping(value="/form")
    public String saveClient (@Valid Client client, BindingResult result, Model model, SessionStatus status){
        if(result.hasErrors()) {
            model.addAttribute("title","Client form");
            return "form";
        }
        clientService.save(client);
        status.setComplete();
        return "redirect:listClients";
    }

    @GetMapping(value="/form/{id}")
    public String editClient(@PathVariable(value = "id") Long id, Model model){
        Client client = null;
        if(id > 0){
            client = clientService.findOne(id);
        } else {
            return "redirect:/listClients";
        }
        model.addAttribute("title","Client form");
        model.addAttribute("client",client);
        return "form";
    }

    @GetMapping(value = "/delete/{id}")
    public String delete(@PathVariable(value = "id") Long id){
        if(id>0){
            clientService.delete(id);
        }
        return "redirect:/listClients";
    }
}
